package org.mule.extension.cxml.converter.internal.internal.extension;

import org.mule.extension.cxml.converter.internal.internal.config.CxmlConverterConfiguration;
import org.mule.runtime.extension.api.annotation.Extension;
import org.mule.runtime.extension.api.annotation.Configurations;
import org.mule.runtime.extension.api.annotation.dsl.xml.Xml;


/**
 * This is the main class of an extension, is the entry point from which configurations, connection providers, operations
 * and sources are going to be declared.
 */
@Xml(prefix = "cxml-converter")
@Extension(name = "Cxml-converter")
@Configurations(CxmlConverterConfiguration.class)
public class CxmlConverterExtension {

}
