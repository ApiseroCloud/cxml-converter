package org.mule.extension.cxml.converter.internal.internal.config;

import org.mule.extension.cxml.converter.internal.internal.connection.provider.CxmlConverterConnectionProvider;
import org.mule.extension.cxml.converter.internal.internal.operations.CxmlConverterOperations;
import org.mule.runtime.extension.api.annotation.Operations;
import org.mule.runtime.extension.api.annotation.connectivity.ConnectionProviders;
import org.mule.runtime.extension.api.annotation.param.Parameter;

/**
 * This class represents an extension configuration, values set in this class are commonly used across multiple
 * operations since they represent something core from the extension.
 */
@Operations(CxmlConverterOperations.class)
@ConnectionProviders(CxmlConverterConnectionProvider.class)
public class CxmlConverterConfiguration {

  @Parameter
  private String configId;

  public String getConfigId(){
    return configId;
  }
}
